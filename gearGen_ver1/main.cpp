#include "xmlParser.h"
#include <vector>
#include <math.h>
#include <string>
#include <sstream>
#include <iostream>

#define PI 3.14159265
#define CONVERSION 72 //pixels per inch
#define SCREWHOLE 0.1640 //inches


struct PairDouble
{
	double r;
	double a;
	PairDouble(double _r, double _a):r(_r),a(_a){};
};


int main()
{

	std::vector<PairDouble> myList;

	//generate all that stupid information
	//constants
	bool straight = false;
	double shaftSize = 0.1640;
	double teethInch = 12; //__ teeth per inch, tooth width = pi*diam/#teeth, tooth radians = #teeth/(2pi) 
	double dedendum = .06; //dedendum in inches, measured down from the pitch circle (radius)
	double adendum = .06; //adendum in inches, measured up from the pitch circle
	double toothThickness = .48; //ratio of tooth thickness and pitch circle to tooth width
	double adendumThickness = .25; //ratio of adendum thicknes to tooth thickness
	//diameter of gear we want. Needs to be multiple of 1/teethInch inches
	//this is really the radius, oops
	//for straight mode, this is equivalent to length
	double diameter = 2;
	double arc = 99999;


	//load settings
	XMLNode myNode = XMLNode::openFileHelper("settings.xml", "settings");
	straight = (bool)atoi(myNode.getChildNode("straight").getText());
	shaftSize = atof(myNode.getChildNode("shaftSize").getText());
	teethInch = atoi(myNode.getChildNode("teethInch").getText());
	dedendum = atof(myNode.getChildNode("dedendum").getText());
	adendum = atof(myNode.getChildNode("adendum").getText());
	toothThickness = atof(myNode.getChildNode("toothThickness").getText());
	adendumThickness = atof(myNode.getChildNode("adendumThickness").getText());
	diameter = atof(myNode.getChildNode("diameter").getText());
	arc = atof(myNode.getChildNode("arc").getText());
	//clean out the node so we can use it again
	myNode.deleteNodeContent();


	//calculate some standard stuff
	int numberTeeth = (int) (teethInch*abs(diameter)); //this better be an int
	double radiansPerTooth = (PI)*2/numberTeeth;

	//create the gear
	for(int i = 0; i < numberTeeth; i++)
	{
		double start = radiansPerTooth*i;

		//this brings the base out to lie on the same normal line as the pitch circle
		double baseOffset = 0;
		if(toothThickness>0.5)
			baseOffset = radiansPerTooth*(abs(0.5-toothThickness))/2;
		
		PairDouble pt1(diameter/2 - dedendum, start - baseOffset);
		PairDouble pt2(diameter/2, start + radiansPerTooth*(0.5-toothThickness)/2);
		PairDouble pt3(diameter/2 + adendum, start + radiansPerTooth*(.5-adendumThickness)/2);
		PairDouble pt4(diameter/2 + adendum, start + radiansPerTooth*( ((.5-adendumThickness)/2) + adendumThickness));
		PairDouble pt5(diameter/2, start + radiansPerTooth*( ((0.5-toothThickness)/2) + toothThickness));
		PairDouble pt6(diameter/2 - dedendum, start + radiansPerTooth*.5 + baseOffset);
		
		myList.push_back(pt1);
		myList.push_back(pt2);
		myList.push_back(pt3);
		myList.push_back(pt4);
		myList.push_back(pt5);
		myList.push_back(pt6);
	}


	//create string to write to file
	std::stringstream myString;

	if(!straight)
	{
		myString << " M " << myList[0].r*CONVERSION*cos(myList[0].a) << " " << myList[0].r*CONVERSION*sin(myList[0].a) << " ";
		for(int i = 1; i < myList.size(); i++)
		{
			myString << " L ";
			myString << myList[i].r*CONVERSION*cos(myList[i].a);
			myString << " ";
			myString << myList[i].r*CONVERSION*sin(myList[i].a);
			if(myList[i].a > uarc*PI/360)
				break;
		}
		myString << " z";
	}
	else
	{
		myString << " M " << (diameter-myList[0].r)*CONVERSION << " " << CONVERSION*myList[0].a*diameter/2 << " ";
		for(int i = 1; i < myList.size(); i++)
		{
			myString << " L ";
			myString << (diameter-myList[i].r)*CONVERSION;
			myString << " ";
			myString << CONVERSION*myList[i].a*diameter/2;
		}
		myString << " z";
	}

	XMLNode svgNode = myNode.addChild("svg");
	XMLNode pathNode = svgNode.addChild("path");
	pathNode.addAttribute("fill","none");
	pathNode.addAttribute("stroke","#000000");
	pathNode.addAttribute("d", myString.str().c_str());



	//if round, then we draw the middle circle
	if(!straight)
	{
		XMLNode circleNode = svgNode.addChild("circle");
		circleNode.addAttribute("fill","none");
		circleNode.addAttribute("stroke","#000000");
		circleNode.addAttribute("cx","0");
		circleNode.addAttribute("cy","0");
		//get the radius
		std::stringstream radiusString;
		radiusString << shaftSize*CONVERSION/2;
		circleNode.addAttribute("r",radiusString.str().c_str());
	}	
	
	myNode.writeToFile("gear.svg");


	return 0;
}