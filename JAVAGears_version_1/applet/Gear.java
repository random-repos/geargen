package runPack;
import processing.core.*;
import java.util.List;
import java.util.LinkedList;

public class Gear extends PApplet{
	public void setup()
	{
		size(800,600);
		randomSeed(hour()+second()+minute());
		//noLoop();
	}
	
	//constants
	double teethInch = 15; //__ teeth per inch, tooth width = pi*diam/#teeth, tooth radians = #teeth/(2pi) 
	double dedendum = .1; //dedendum in inches, measured down from the pitch circle (radius)
	double adendum = .1; //adendum in inches, measured up from the pitch circle
	double toothThickness = .48; //ratio of tooth thickness and pitch circle to tooth width
	double adendumThickness = .25; //ratio of adendum thicknes to tooth thickness
	
	//diameter of gear we want. Needs to be multiple of 1/teethInch inches
	double diameter = 2;

	//our list of points
	List<PairDouble> myList = new LinkedList<PairDouble>();
	public void draw()
	{
		background(200);
		println((double)diameter);
		
		
		
		//calculate some useful constants
		int numberTeeth = (int) (teethInch*diameter); //this better be an int
		double radiansPerTooth = (Math.PI)*2/numberTeeth;
		//generate list of points
		//for each tooth
		myList.clear();
		for(int i = 0; i < numberTeeth; i++)
		{
			double start = radiansPerTooth*i;
			
			
			
			PairDouble pt1 = new PairDouble(diameter - dedendum, start);
			PairDouble pt2 = new PairDouble(diameter, start + radiansPerTooth*(0.5-toothThickness)/2);
			PairDouble pt3 = new PairDouble(diameter + adendum, start + radiansPerTooth*(.5-adendumThickness)/2);
			PairDouble pt4 = new PairDouble(diameter + adendum, start + radiansPerTooth*( ((.5-adendumThickness)/2) + adendumThickness));
			PairDouble pt5 = new PairDouble(diameter, start + radiansPerTooth*( ((0.5-toothThickness)/2) + toothThickness));
			PairDouble pt6 = new PairDouble(diameter - dedendum, start + radiansPerTooth*.5);
			
			myList.add(pt1);
			myList.add(pt2);
			myList.add(pt3);
			myList.add(pt4);
			myList.add(pt5);
			myList.add(pt6);
		}
		
		translate(250,250);
		beginShape();
		fill(255);
		for(PairDouble pt : myList)
		{
			vertex((float)(pt.r*100*Math.cos(pt.a)), (float)(pt.r*100*Math.sin(pt.a)));
		}
		endShape(CLOSE);
	}
	
	private int iRand(int size)
	{
		return (int)random(size*2)%size+1;
	}
	
	public void keyPressed()
	{
		if(keyCode == UP)
			diameter += 1.0/15.0;
		else if(keyCode == DOWN)
			diameter -= 1.0/15.0;
	}
}
